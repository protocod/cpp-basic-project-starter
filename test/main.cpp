#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
#include "foo.hpp"

TEST_CASE("test greeter function") {
  auto expected = std::string("Hello world!");
  REQUIRE(!expected.compare(greeter()));
}
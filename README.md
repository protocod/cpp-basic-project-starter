# C++ Basic Project Starter

To create a C++ project from an existing project structure including usefull tools to make your life better.

This project starter includes:

* A clean project structure
* CMake for building
* Catch2 for testing
* Doxygen for documentation
* Support for `vscode` using C/C++ plugin made by Microsoft

## Build project

CMake is used to generate a Makefile used to build the entire project, including tests.

### Build for production

``` shell
cd build
cmake ../ -DCMAKE_BUILD_TYPE=Release && make
```
### Build for development

For debugging use the `Debug` mode.

``` shell
cd build
cmake ../ -DCMAKE_BUILD_TYPE=Debug && make
```

## Run tests

Tests are built in `bin/testing` file. You can simply run tests using this command:

``` shell
bin/testing
```

## Build the documentation

Generate an `html` and `latex` documentation of your project using doxygen.

Install doxygen and execute:

``` shell
cd doc
doxygen config
```

You can configure doxygen behavior by editing the file `doc/config`.

## For vscode users

The `.vscode` directory contains tasks and a launch configuration to build and test the project.

From `.vscode/tasks.json`

``` json
{
  // See https://go.microsoft.com/fwlink/?LinkId=733558
  // for the documentation about the tasks.json format
  "version": "2.0.0",
  "tasks": [
    {
      "label": "cmake:debug",
      "type": "shell",
      "command": "cd build && cmake ../ -DCMAKE_BUILD_TYPE=Debug && make"
    },
    {
      "label": "cmake:release",
      "type": "shell",
      "command": "cd build && cmake ../ -DCMAKE_BUILD_TYPE=Release && make"
    },
    {
      "label": "catch2:test",
      "type": "shell",
      "command": "bin/testing"
    },
    {
      "label": "run:dev",
      "type": "shell",
      "dependsOrder": "sequence",
      "dependsOn": [
        "cmake:debug",
        "catch2:test"
      ],
    },
    {
      "label": "run:build",
      "type": "shell",
      "dependsOrder": "sequence",
      "dependsOn": [
        "cmake:release",
        "catch2:test"
      ],
    }
  ]
}
```

From `.vscode/launch.json`

``` json
{
  // Use IntelliSense to learn about possible attributes.
  // Hover to view descriptions of existing attributes.
  // For more information, visit: https://go.microsoft.com/fwlink/?linkid=830387
  "version": "0.2.0",
  "configurations": [
    {
      "name": "C++ Development",
      "type": "cppdbg",
      "request": "launch",
      "program": "${workspaceFolder}/bin/app",
      "args": [],
      "stopAtEntry": false,
      "cwd": "${workspaceFolder}",
      "environment": [],
      "externalConsole": false,
      "MIMode": "gdb",
      "setupCommands": [
        {
          "description": "Activer l'impression en mode Pretty pour gdb",
          "text": "-enable-pretty-printing",
          "ignoreFailures": true
        }
      ],
      "preLaunchTask": "run:dev",
      "miDebuggerPath": "/usr/bin/gdb"
    },
    {
      "name": "C++ Production",
      "type": "cppdbg",
      "request": "launch",
      "program": "${workspaceFolder}/bin/app",
      "args": [],
      "stopAtEntry": false,
      "cwd": "${workspaceFolder}",
      "environment": [],
      "externalConsole": false,
      "MIMode": "gdb",
      "setupCommands": [
        {
          "description": "Activer l'impression en mode Pretty pour gdb",
          "text": "-enable-pretty-printing",
          "ignoreFailures": true
        }
      ],
      "preLaunchTask": "run:build",
      "miDebuggerPath": "/usr/bin/gdb"
    }
  ]
}
```